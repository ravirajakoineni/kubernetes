git clone https://ravirajakoineni@bitbucket.org/ravirajakoineni/kubernetes.git

1. Ubuntu 18.4 LTS with 2 GB  and 2 CPU  — static ip and set hostname permanently.
2.  Install openssh  and Ansible 
3. Generate ssh keys
4. Give sudo permissions to user and edit visudo for NOPASSWD
5. Copy KUBERNETES_CLUSTER on the machine
6. Run the play books  
        Docker.yaml
        Kubernetes.yaml


MASTER HOSTNAME: kubemaster 
STATIC IP:   10.0.2.15     
USER: kubeadmin


WORKER HOSTNAME: worker1
STATIC IP: 10.0.2.16
USER: kubeadmin
